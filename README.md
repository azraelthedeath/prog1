#### Dúvidas?
Primeiro leia a [Wiki](https://github.com/azraelthedeath/prog-1/wiki), caso sua dúvida não seja sanada por lá, abra um [issue](https://github.com/azraelthedeath/prog-1/issues/new?assignees=azraelthedeath&labels=duvida).

*NO WINDOWS, **TROQUE***  
```C
  system("read -n1 -r -p 'Pressione qualquer tecla para continuar...' key");
  system("clear");
```  
```C
  for(int i = 0; ...
```
*POR*

```C
  system("pause");  
  system("cls");
```  
```C
  int i;
  for(i = 0; ...
```
