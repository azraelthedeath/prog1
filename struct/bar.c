#include <stdio.h>
#include <stdlib.h>
#define tf 100

  //define struct bar
  typedef struct {
    int mesas[tf][tf], tll, tlc;
    double precos[tf], diario[tf];
  } bar;

  //função para inserção de dados na matriz de mesas
  bar mesas(bar *b)
  {
    printf("Quantas mesas? ");  scanf("%d", &b->tlc);
    printf("Quantos produtos? ");  scanf("%d", &b->tll);
    for(int i = 0; i < b->tlc; i++)
    {
      for(int j = 0; j < b->tll; j++)
      {
        printf("mesa %d produto %d: ", i, j);
        scanf("%d", &b->mesas[j][i]);
      }//produto
    }//mesa
  }//função

  //função para inserção de dados no vetor de preços
  bar precos(bar *b)
  {
    for(int i = 0; i < b->tll; i++)
    {
      printf("produto %d: ", i);
      scanf("%lf", &b->precos[i]);
    }//preço
  }//função

  //função para exibir a matriz de mesas e o vetor de preços
  bar exibe(bar b)
  {
    for(int i = 0; i < b.tll; i++)
    {
      for(int j = 0; j < b.tlc; j++)
      {
        printf("%.3d ", b.mesas[i][j]);
      }//produto
      printf("\n");
    }//mesa
    printf("\n");
    for(int i = 0; i < b.tll; i++)
    {
      printf("produto %.2d: R$ %.2lf\n", i, b.precos[i]);
    }//preço
  }//função

  //função para fechar a conta de uma mesa
  //não precisa de ponteiro, pois não passará nenhum valor pro struct
  bar conta(bar b)
  {
    double subtotal = 0, desconto, total;
    int mesa;
    printf("Mesa a ser calculada: ");  scanf("%d", &mesa);
    printf("MESA %d\n", mesa);
    for(int i = 0; i < b.tll; i++)
    {
      subtotal += (b.mesas[i][mesa] * b.precos[i]);
      printf("produto %d: R$ %.2lf\n", i, b.mesas[i][mesa] * b.precos[i]);
    }//produto
    printf("Subtotal: R$ %.2lf\n", subtotal);
    printf("Desconto (porcentagem): ");  scanf("%lf", &desconto);
    total = subtotal - ((desconto / 100) * subtotal);
    printf("Total: R$ %.2lf\n", total);
    b.diario[mesa] = total;
  }//função

  //função para fechar a conta de todas as mesas
  //também não precisa de ponteiro, pois, como ja falado, não será inserido nada no struct
  bar fechar(bar b)
  {
    double dia = 0;
    for(int i = 0; i < b.tll; i++)
    {
      for(int j = 0; j < b.tlc; j++)
      {
        dia += b.mesas[i][j] * b.precos[i];
      }//coluna
    }//linha
    printf("Total: R$ %.2lf\n", dia);
    printf("ESTE TOTAL NAO SE ENCONTRA COM OS DEVIDOS DESCONTOS.\n");
  }//função

  int main()
  {
    int menu = 999;
    bar b;
    while(menu != 0)
    {
      printf("MENU\n");
      printf("1 - Criar tabela de mesas.\n");
      printf("2 - Criar tabela de preços do dia.\n");
      printf("3 - Exibir tabelas.\n");
      printf("4 - Fechar mesa.\n");
      printf("5 - Fechar bar.\n");
      printf("0 - Sair.\n");
      printf("\nSelecione a função: ");
      scanf("%d", &menu);

      switch(menu)
      {
        case 1:
          mesas(&b);
          system("read -n1 -r -p 'Pressione qualquer tecla para continuar...' key");
          system("clear");
        break;
        case 2:
          precos(&b);
          system("read -n1 -r -p 'Pressione qualquer tecla para continuar...' key");
          system("clear");
        break;
        case 3:
          exibe(b);
          system("read -n1 -r -p 'Pressione qualquer tecla para continuar...' key");
          system("clear");
        break;
        case 4:
          conta(b);
          system("read -n1 -r -p 'Pressione qualquer tecla para continuar...' key");
          system("clear");
        break;
        case 5:
          fechar(b);
          system("read -n1 -r -p 'Pressione qualquer tecla para continuar...' key");
          system("clear");
        break;
      }//switch
    }//while
    return 0;
  }//main
